#  Copyright (c) 2020 Chris Stranex
#  See LICENSE for licencing information.
#
#  There is NO WARRANTY, to the extent permitted by law.
#

from unittest import TestCase

from alicorn.dependency import inject, Depends
from alicorn._core import Alicorn


class DependencyTest(TestCase):
    """Test dependency injector"""

    def test_inject_by_type(self):
        def callback():
            return 1

        @inject()
        def test(cb: callback = Depends):
            return cb

        self.assertEqual(test(), 1)

        value = 2

        @inject()
        def test(cb: value = Depends):
            return cb

        self.assertEqual(test(), 2)

    def test_inject_by_callable(self):
        def callback():
            return 1

        @inject()
        def test(cb=Depends(callback)):
            return cb

        self.assertEqual(test(), 1)

    def test_non_injected(self):
        def callback():
            return 1

        @inject()
        def test(cb: callback):
            return cb

        self.assertEqual(test(2), 2)
        self.assertEqual(test(None), None)

    def test_nested_injecting(self):
        def callbackB():
            return 2

        def callbackA(cb: callbackB = Depends):
            return cb

        @inject()
        def test(cb: callbackA = Depends):
            return cb + 1

        self.assertEqual(test(), 3)

    def test_registered_dependency(self):
        def callback():
            return 1

        a = Alicorn()
        a.add_dependency('test', callback)

        @a.dependency
        def callbackB():
            return 2

        @inject(a)
        def test(cb=Depends('test')):
            return cb

        self.assertEqual(test(), 1)

        @inject(a)
        def test(cb=Depends('callbackB')):
            return cb

        self.assertEqual(test(), 2)

#  Copyright (c) 2020 Chris Stranex
#  See LICENSE for licencing information.
#
#  There is NO WARRANTY, to the extent permitted by law.
#

from unittest import TestCase
from unittest.mock import patch
from alicorn._config import Config


class ConfigTest(TestCase):

    def test_nested_set(self):
        c = Config()
        c._nested_set('a.b.c', True)
        self.assertTrue(c['a.b.c'])
        c._nested_set('a.b.d', False)
        self.assertFalse(c['a.b.d'])
        self.assertDictEqual(c['a.b'], {
            'c': True,
            'd': False
        })

    def test_load_default(self):
        cfg = Config({
            'grpc': {
                'compression': True
            }
        })
        self.assertIn('grpc', cfg)
        self.assertIn('grpc.compression', cfg)
        self.assertIn('compression', cfg['grpc'])
        self.assertEqual(cfg['grpc.compression'], True)
        self.assertEqual(cfg.get('grpc.compression'), True)

    def test_load_global(self):
        global_config = Config({
            'grpc': {
                'compression': False,
                'maximum_concurrent_rpcs': 1
            }
        })
        cfg = Config({
            'grpc': {
                'compression': True
            }
        }, global_config=global_config)
        self.assertEqual(cfg['grpc.compression'], False)
        self.assertEqual(cfg['grpc.maximum_concurrent_rpcs'], 1)

    def test_load_environ(self):
        environ = {
            'ALICORN_GRPC__COMPRESSION': False,
            'ALICORN_GRPC__MAXIMUM_CONCURRENT_RPCS': '1',
            'ALICORN_UNKNOWN_OPTION': 'test',
            'RANDOM_ENVIRON': True
        }

        c = Config()
        with patch('os.environ', new_callable=lambda: environ):
            c.load_from_environ()
        self.assertIn('grpc.compression', c, c)
        self.assertFalse(c['grpc.compression'])
        self.assertEqual(c['grpc.maximum_concurrent_rpcs'], '1')
        self.assertEqual(c['unknown_option'], 'test')
        self.assertNotIn('random_environ', c)

    def test_enviorn_mapping(self):
        environ = {
            'ALICORN_COMPRESSION': True
        }

        def mapper(key):
            return f'grpc.{key}'

        c = Config()
        with patch('os.environ', new_callable=lambda: environ):
            c.load_from_environ(mapping=mapper)

        self.assertTrue(c['grpc.compression'])
        self.assertNotIn('compression', c)

    def test_convert(self):
        c = Config({
            'integer': '1'
        })
        self.assertIsInstance(c.get('integer', None, int), int)
        self.assertIsNone(c.get('string', None, str))

    def test_load_dict(self):
        config = {
            'grpc': {
                'compression': True
            }
        }
        c = Config()
        c.load_from_dict(config)
        self.assertTrue(c.get('grpc.compression'))

        config = {
            'enabled': True
        }
        c.load_from_dict(config, prefix='logging')
        self.assertTrue(c.get('logging.enabled'))

#  Copyright (c) 2020 Chris Stranex
#  See LICENSE for licencing information.
#
#  There is NO WARRANTY, to the extent permitted by law.
#

from unittest import TestCase
from alicorn import Alicorn
from .proto_pb2 import HelloWorldServiceServicer, HelloWorldResponse, HelloWorldRequest, DESCRIPTOR
from grpc import StatusCode
from grpc_testing import server_from_dictionary, strict_fake_time


class PythonServiceTest(TestCase):

    def setUp(self):
        self.alicorn = Alicorn()

    def test_naming(self):
        @self.alicorn.rpc_service()
        class TestService:
            pass

        self.assertIn('TestService', self.alicorn._rpc_services)

        @self.alicorn.rpc_service('MyService')
        class AnotherSerivce:
            pass

        self.assertIn('MyService', self.alicorn._rpc_services)
        self.assertNotIn('AnotherService', self.alicorn._rpc_services)

    def test_unary_rpc_service(self):
        """Test """

    def test_stream_rpc_service(self):
        """"""


class GrpcServiceTest(TestCase):

    def setUp(self):
        self.alicorn = Alicorn()

    def test_method_decorated(self):
        """Test that the method has been decorated with _request_handler and inject"""

        @self.alicorn.service
        class TestService(HelloWorldServiceServicer):
            pass

        # This is just a basic test for now to makes ure the method was wrapped
        assert hasattr(TestService.SayHello, '__wrapped__')

        def test():
            return 1

    def test_unary_service(self):
        @self.alicorn.service
        class TestService(HelloWorldServiceServicer):
            def SayHello(self, request: HelloWorldRequest, context):
                return HelloWorldResponse(message=request.name)

        request = HelloWorldRequest(name='Test')

        server_time = strict_fake_time(0)
        test_server = server_from_dictionary(self.alicorn.get_handlers(), server_time)
        method = test_server.invoke_unary_unary(
            method_descriptor=DESCRIPTOR.services_by_name['HelloWorldService'].methods_by_name['SayHello'],
            invocation_metadata={},
            request=request,
            timeout=1
        )

        response, metadata, code, details = method.termination()
        self.assertEqual(response.message, request.name)
        self.assertEqual(code, StatusCode.OK)

    def test_stream_service(self):
        pass

    def test_non_method(self):
        """Test a non-grpc method is annotated"""

        @self.alicorn.service
        class TestService(HelloWorldServiceServicer):
            def non_method(self):
                return True

        assert not hasattr(TestService.non_method, '__wrapped__'), "non_method has a decorator"


class ContextServiceTest(TestCase):

    def setUp(self):
        self.alicorn = Alicorn()

    def test_context(self):
        pass

    def test_before_request(self):
        pass

    def test_after_request(self):
        pass

    def test_after_teardown(self):
        pass

Api Reference
=============
.. automodule:: alicorn

    .. autoclass:: Alicorn
        :exclude-members: service, dependency, inject, rpc_service, rpc_method_unary, rpc_method_unary_stream, rpc_method_stream_unary, rpc_method_stream
        :members:

        .. autodecorator:: alicorn.Alicorn.service

        .. autodecorator:: alicorn.Alicorn.dependency

        .. autodecorator:: alicorn.Alicorn.inject

        .. autodecorator:: alicorn.Alicorn.rpc_service

        .. autodecorator:: alicorn.Alicorn.rpc_method_unary

        .. autodecorator:: alicorn.Alicorn.rpc_method_unary_stream

        .. autodecorator:: alicorn.Alicorn.rpc_method_stream_unary

        .. autodecorator:: alicorn.Alicorn.rpc_method_stream

Discovery
---------
.. autofunction:: alicorn.auto_discover

Extension
---------
.. automodule:: alicorn.extension
    :members:

Dependencies
------------
.. automodule:: alicorn.dependency
    :members:

Ports
-----
.. automodule:: alicorn.port
    :members:

Signals
-------
.. automodule:: alicorn.signal
    :members:

Exceptions
----------
.. automodule:: alicorn.exception
   :members:

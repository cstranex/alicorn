Alicorn - The grpc framework for Python
=======================================

Alicorn is a grpc framework to easily create grpc services.

Features:
 - Dependency Injection
 - Python and .proto service creation
 - Before/After request handlers
 - Extensions

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   api/reference

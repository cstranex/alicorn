#  Copyright (c) 2020 Chris Stranex
#  See LICENSE for licencing information.
#
#  There is NO WARRANTY, to the extent permitted by law.
#

from typing import TypeVar

from google.protobuf.message import Message

ProtobufMessage = Message

RequestType = TypeVar('RequestType')

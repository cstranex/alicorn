#  Copyright (c) 2020 Chris Stranex
#  See LICENSE for licencing information.
#
#  There is NO WARRANTY, to the extent permitted by law.
#

def run(args):
    """Load and run an alicorn grpc server. Arguments are automatically turned into configuration.

    Resolution for configuration:
        - Default Config
        - Environment Variables
        - User supplied configuration
        - Command Line Options
    """

    # FIXME: Finish implementation
    raise RuntimeError("Running alicorn from the module is not yet supported")
